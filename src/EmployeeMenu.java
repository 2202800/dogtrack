import Object.Dog;
import Object.Services;

import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class EmployeeMenu {
    private static Connection employeeConnection;
    private static String paymentID;
    private static CustomerMenu cm;

    //the code/method below sets the connection to sql/phpmyadmin
    public static void setConnection() {
        try {
            String connectionURL = "jdbc:mysql://localhost/dogtrack?user=root&password=";
            employeeConnection = DriverManager.getConnection(connectionURL);

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    //the code/method below closes the connection
    public static  void closeConnection() throws SQLException {
        if (employeeConnection != null) {
            employeeConnection.close();
        }
    }

    //the code below shows the Employee menu
    public static void employeeMenu() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println();
            System.out.println("Choose Options:");
            System.out.println("[1] View Dog/s");
            System.out.println("[2] View Service/s");
            System.out.println("[3] View Service/s by availability");
            System.out.println("[4] Add service");
            System.out.println("[5] Update service");
            System.out.println("[6] Delete service");
            System.out.println("[7] Receive Payment");
            System.out.println("[8] Exit");
            System.out.println("===============================");
            System.out.println("Enter Choice: ");
            int choice = scanner.nextInt();

            if (choice == 1) {
                ArrayList<Dog> customersDog;
                customersDog = ViewDogsEmployee();
                System.out.println();
                System.out.println("The customer's dog/s are as follows:");
                for (Dog dog : customersDog) {
                    System.out.printf("%-5s %-15s %-10s %-5s %-15s \n", dog.getPetID(), dog.getPetName(),
                            dog.getBreed(), dog.getCustomerID(), dog.getDogPicture());

                }
            }else if (choice == 2) {
                ViewServices();
            } else if (choice == 3) {
                ViewAvailableServices();
            } else if (choice == 4) {
                addService();
            } else if (choice == 5) {
                updateServiceMenu();
            } else if (choice == 6) {
                deleteService();
            } else if (choice == 7) {
                if(cm.sumAmount==0){
                    System.out.println("0 Balance for all customer!");
                    employeeMenu();
                }else {
                    Scanner scanner1 = new Scanner(System.in);
                    System.out.println("Would you like to pay your transactions? ");
                    System.out.println("Y/N: ");
                    String input = scanner1.nextLine();
                    if (input.equals("Y") || input.equals("y")) {
                        receivePayment();
                    } else if (input.equals("N") || input.equals("yn")) {
                        employeeMenu();
                    }
                }

            } else if (choice == 8){
                scanner.close();
                Login.closeConnection();
                Registration.closeConnection();
                EmployeeMenu.closeConnection();
                System.exit(0);
            }
        }
    }

    //the code method below views the list of services
    public static void ViewServices() throws SQLException {
        String sql = "SELECT * FROM services";
        PreparedStatement preparedStatement = employeeConnection.prepareStatement(sql);

        ResultSet rs = preparedStatement.executeQuery();

        System.out.println("Services:");
        while (rs.next())
        {

            String ServiceID = rs.getString("ServiceID");
            String ServiceName = rs.getString("ServiceName");
            String ServiceDescription = rs.getString("ServiceDescription");
            String Price = rs.getString("Price");
            String ServiceStatus = rs.getString("ServiceStatus");


            // print the results
            System.out.format("%-5s %-15s PHP%-7s %-10s %s\n", ServiceID, ServiceName, Price, ServiceStatus, ServiceDescription);
        }

        System.out.println();
    }

    //the code method below views the list of customer who have balance
    public static void receivePayment() throws SQLException {

        Login login = new Login();
        cm.setConnection();

        Scanner keyboard = new Scanner(System.in);
        String sql = "SELECT payment.paymentamount, payment.customerid FROM payment WHERE paymentamount > 0 ";
        PreparedStatement preparedStatement = employeeConnection.prepareStatement(sql);

        ResultSet rs = preparedStatement.executeQuery();

        System.out.println("Customer Balance:");
        while (rs.next())
        {

            int paymentamount = rs.getInt("paymentamount");
            String customerid = rs.getString("customerid");

            // print the results
            System.out.format("%-5d %-15s\n", paymentamount, customerid);
        }

        System.out.println();

        System.out.println("Choose a Customer to receive the payment: " );
        String customerID = keyboard.next();

        System.out.println("Would you like to pay through Gcash or Cash: ");
        String paymentType = keyboard.next();

        String registerQuery = "INSERT INTO paymentdetails values(?,?,?,?)";
        PreparedStatement registerPayment = employeeConnection.prepareStatement(registerQuery);

        String latestPaymentID = cm.getLatestPaymentID();
        String[] splitToIncrement = latestPaymentID.split("PA");
        int incrementValue = Integer.parseInt(splitToIncrement[1]);
        System.out.println(incrementValue);
        incrementValue++;
        if(incrementValue>11){
            incrementValue++;
        }
        System.out.println(incrementValue);
        paymentID = "PA"+incrementValue;

        System.out.println(paymentID);
        registerPayment.setString(1, paymentID);
        registerPayment.setString(2, cm.getTransactionID(customerID));
        registerPayment.setString(3, paymentType);
        registerPayment.setDouble(4, cm.getSumAmount());


            String sql2 = "UPDATE payment SET employeeid = '"+login.getEmployeeID()+
                    "' ,paymentamount = " + 0+ " WHERE CustomerID = '"
                    +customerID+"'";
            PreparedStatement preparedStatement1 = employeeConnection.prepareStatement(sql2);
            preparedStatement1.executeUpdate();



        System.out.println("Customer: " + customerID + " Paid! ");



    }


    //the code/method below shows all the available services
    public static void ViewAvailableServices() throws SQLException {

        String sql = "SELECT ServiceID, ServiceName, ServiceDescription, Price FROM services WHERE ServiceStatus = ?";
        PreparedStatement preparedStatement = employeeConnection.prepareStatement(sql);
        preparedStatement.setString(1, "Available");

        ResultSet rs = preparedStatement.executeQuery();

        System.out.println("Available Services are as follows:");
        while (rs.next())
        {

            String ServiceID = rs.getString("ServiceID");
            String ServiceName = rs.getString("ServiceName");
            String ServiceDescription = rs.getString("ServiceDescription");
            String Price = rs.getString("Price");


            // print the results
            System.out.format("%-5s %-15s PHP%-7s %s\n", ServiceID, ServiceName, Price, ServiceDescription);
        }

        System.out.println();
    }

    //the code below views the list of dogs per customer
    public static ArrayList<Dog> ViewDogsEmployee() {
        Scanner keyboard = new Scanner(System.in);
        ArrayList<Dog> customersDog = new ArrayList<>();

        try {
            System.out.println("Enter customer id: ");
            String customerID = keyboard.next();

            Statement stmt = employeeConnection.createStatement();
            String sql = "SELECT * FROM dog WHERE CustomerID = '" + customerID + "'";
            stmt.execute(sql);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Dog dog = new Dog(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5));

                customersDog.add(dog);
            }

            stmt.close();
            rs.close();
        }catch (Exception e) {
            System.err.println("Customer ID does not exists.");
        }

        return customersDog;
    }

    //the code below adds a service by an employee
    public static void addService() {
        try {
            Statement statement = employeeConnection.createStatement();

            Services newService = new Services();
            Scanner keyboard = new Scanner(System.in);

            System.out.println("Enter Service ID: ");
            String serviceID = keyboard.nextLine();
            newService.setServiceID(serviceID);

            System.out.println("Enter Service Name: ");
            String serviceName = keyboard.nextLine();
            newService.setServiceName(serviceName);

            System.out.println("Enter Service Description: ");
            String serviceDescription = keyboard.nextLine();
            newService.setServiceDescription(serviceDescription);

            System.out.println("Enter Price: ");
            int price = keyboard.nextInt();
            newService.setPrice(price);

            statement.executeUpdate("INSERT INTO services(ServiceID, ServiceName, ServiceDescription, Price)" +
                    "VALUES ('"+newService.getServiceID()+"', '"+ newService.getServiceName()+"', '"+newService.getServiceDescription()+"', '"+newService.getPrice()+"')");

            System.out.println("Service successfully added");

            statement.close();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    //The code/method below updates the service menu by the employee
    public static void updateServiceMenu() throws SQLException {
        Scanner scanner = new Scanner(System.in).useDelimiter("\\n");

        while (true) {
            System.out.println();
            System.out.println("Choose the service data to be updated:");
            System.out.println("[1] Service name");
            System.out.println("[2] Service description");
            System.out.println("[3] Price");
            System.out.println("[4] Set status");
            System.out.println("[5] Back");
            System.out.println("===============================");
            System.out.println("Enter Choice: ");
            int choice = scanner.nextInt();

            if (choice == 1) {
                System.out.println("Enter old service name: ");
                String oldServiceName = scanner.next();

                System.out.println("Enter new service name: ");
                String newServiceName = scanner.next();

                String updateQueryName = "UPDATE services SET ServiceName = ? WHERE ServiceName = ?";

                PreparedStatement preparedStatement = employeeConnection.prepareStatement(updateQueryName);
                preparedStatement.setString(1, newServiceName);
                preparedStatement.setString(2, oldServiceName);
                preparedStatement.executeUpdate();
                System.out.println("Successfully updated service name");
                preparedStatement.close();

            } else if (choice == 2) {
                // needs an update
                System.out.println("Enter service id of the service description: ");
                String serviceID = scanner.next();

                System.out.println("Enter new service description: ");
                String newServiceDescription = scanner.next();


                String updateQueryDescription = "UPDATE services SET ServiceDescription = ? WHERE ServiceID = ?";

                PreparedStatement preparedStatement = employeeConnection.prepareStatement(updateQueryDescription);
                // preparedStatement.set
                preparedStatement.setString(1, newServiceDescription);
                preparedStatement.setString(2, serviceID);
                preparedStatement.executeUpdate();
                System.out.println("Successfully updated service description");
                preparedStatement.close();

            } else if (choice == 3) {
                System.out.println("Enter service id of the service price: ");
                String serviceID = scanner.next();

                System.out.println("Enter new service price: ");
                double newServicePrice = scanner.nextDouble();


                String updatePrice = "UPDATE services SET Price = ? WHERE ServiceID = ?";

                PreparedStatement preparedStatement = employeeConnection.prepareStatement(updatePrice);
                preparedStatement.setDouble(1, newServicePrice);
                preparedStatement.setString(2, serviceID);
                preparedStatement.executeUpdate();
                System.out.println("Successfully updated price");
                preparedStatement.close();

            } else if (choice == 4) {
                String available = "Available";
                String unavailable = "Unavailable";

                System.out.println("Enter service id: ");
                String serviceID = scanner.next();

                System.out.println("Set status to: ");
                System.out.println("[1] Available");
                System.out.println("[2] Unavailable");
                System.out.println("Enter Choice: ");
                int input = scanner.nextInt();

                if (input == 1) {
                    String updateQueryDescription = "UPDATE services SET ServiceStatus = ? WHERE ServiceID = ?";

                    PreparedStatement preparedStatement = employeeConnection.prepareStatement(updateQueryDescription);
                    preparedStatement.setString(1, available);
                    preparedStatement.setString(2, serviceID);
                    preparedStatement.executeUpdate();
                    System.out.println("Successfully updated status");
                    preparedStatement.close();

                } else if (input == 2) {
                    String updateQueryDescription = "UPDATE services SET ServiceStatus = ? WHERE ServiceID = ?";

                    PreparedStatement preparedStatement = employeeConnection.prepareStatement(updateQueryDescription);
                    preparedStatement.setString(1, unavailable);
                    preparedStatement.setString(2, serviceID);
                    preparedStatement.executeUpdate();
                    System.out.println("Successfully updated status");
                }

            } else if (choice == 5) {
                employeeMenu();
            }
        }
    }

    //the code/method below deletes a service
    public static void deleteService() {
        Scanner keyboard = new Scanner(System.in);

        try {
            Services deleteService = new Services();
            System.out.println("Enter service id: ");
            String serviceID = keyboard.next();
            deleteService.setServiceID(serviceID);

            String deleteQuery = "DELETE FROM services WHERE ServiceID = ?";

            PreparedStatement preparedStatement = employeeConnection.prepareStatement(deleteQuery);
            preparedStatement.setString(1, serviceID);
            preparedStatement.executeUpdate();
            System.out.println("Successfully deleted service");
            preparedStatement.close();

        } catch (SQLException sqle) {
            System.err.println("Please enter valid service id");
        }
    }

}
