import java.util.Scanner;

public class RunDogTrack {
    public static void main(String[] args) {

        LoginAndRegistrationMenu();

    }

    //The code below is for the customer and employee login, also for customer registration
    public static void LoginAndRegistrationMenu() {
        Login.setConnection();
        Registration.setConnection();

        while (true) {
            Scanner scanner = new Scanner(System.in);

            Registration registration = new Registration();
            Login login = new Login();

            System.out.println("WELCOME TO DOGTRACK!");
            System.out.println("==================================");
            System.out.println("[1] Customer Registration");
            System.out.println("[2] Customer Login");
            System.out.println("[3] Employee Login");
            System.out.println("[4] Exit");

            System.out.println("Enter Choice:");
            int choice = scanner.nextInt();

            if (choice == 1) {
                registration.CustomerRegistration();
            } else if (choice == 2) {
                login.CustomerLogin();
            } else if (choice == 3) {
                login.EmployeeLogin();

            } else if (choice == 4) {
                System.exit(0);
            }
        }
    }
}
