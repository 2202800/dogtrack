package Object;

public class Services {
    private String serviceID;
    private String serviceName;
    private String serviceDescription;
    private double price;
    private String serviceStatus;
    private String serviceTime;

    public Services() {
    }

    public Services(String serviceID, String serviceName,
                    String serviceDescription, double price,
                    String serviceStatus,String serviceTime) {
    this.serviceID = serviceID;
    this.serviceName = serviceName;
    this.serviceDescription = serviceDescription;
    this.price = price;
    this.serviceStatus = serviceStatus;
    this.serviceTime = serviceTime;

    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getServiceTime(){return serviceTime;}

    public void setServiceTime(String serviceTime){this.serviceTime=serviceTime;}
}
