package Object;

public class Dog {
    private String petID;
    private String petName;
    private String breed;
    private String cutomerID;
    private String dogPicture;

    public Dog(){
    }

    public Dog(String petID, String petName, String breed,
               String customerId, String dogPicture){
        this.petID=petID;
        this.petName=petName;
        this.breed=breed;
        this.cutomerID=customerId;
        this.dogPicture=dogPicture;
    }

    public String getPetID(){return petID;}
    public void setPetID(String petID){this.petID = petID;}

    public String getPetName(){return petName;}
    public void setPetName(String petName){this.petName=petName;}

    public String getBreed(){return breed;}
    public void setBreed(String breed){this.breed= breed;}

    public String getCustomerID(){return cutomerID;}
    public void setCustomerID(String cutomerID){this.cutomerID=cutomerID;}

    public String getDogPicture(){return dogPicture;}
    public void setDogPicture(String dogPicture){this.dogPicture= dogPicture;}
}
