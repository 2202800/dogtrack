package Object;

public class Customer {
    private String customerId;
    private String name;
    private String emailAddress;
    private String homeAddress;
    private String customerPassword;

    public Customer(){
    }

    public Customer(String customerId, String name, String emailAddress,
                    String homeAddress, String customerPassword){
        this.customerId = customerId;
        this.name = name;
        this.emailAddress = emailAddress;
        this.homeAddress = homeAddress;
        this.customerPassword =customerPassword;
    }

    public String getCustomerId(){return customerId;}
    public void setCustomerId(String customerId){this.customerId = customerId;}

    public String getName(){return name;}
    public void setName(String name){this.name=name;}

    public String getEmailAddress(){return emailAddress;}
    public void setEmailAddress(String emailAddress){this.emailAddress=emailAddress;}

    public String getHomeAddress(){return  homeAddress;}
    public void setHomeAddress(String homeAddress){this.homeAddress=homeAddress;}

    public String getCustomerPassword(){return customerPassword;}
    public void setCustomerPassword(String customerPassword){this.customerPassword=customerPassword;}

}
