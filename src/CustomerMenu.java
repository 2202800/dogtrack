
import Object.Customer;
import Object.Services;
import Object.Dog;
import com.mysql.cj.log.Log;

import javax.crypto.CipherInputStream;
import javax.swing.text.View;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;

public class CustomerMenu {
    public static Connection customerMenuConnection;
    public static String serviceID;
    public static double servicePrice;
    public static String petName;
    public static String petID;
    public static double sumAmount;
    public static int numberOfCustomer;
    public static int numberOfDog;
    public static String employeeID;
    public static String employeeName;
    public static int numberOfPayment;
    public static String transactionID;
    public static String paymentID;
    public static String customerID;

    // sets the connection
    public static void setConnection() {
        try {
            String connectionURL = "jdbc:mysql://localhost/dogtrack?user=root&password=";
            customerMenuConnection = DriverManager.getConnection(connectionURL);

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    //closes the connection
    public static void closeConnection() throws SQLException {
        if (customerMenuConnection != null) {
            customerMenuConnection.close();
        }
    }

    //Customer Menu
    public static void customerMenu() throws SQLException {
        Login login = new Login();

        while(true){

            Scanner scanner = new Scanner(System.in);

            System.out.println();
            System.out.println("Choose Options:");
            System.out.println("Customer Number of Dogs: " + countDog());
            System.out.println("Customer ID: " + login.getCustomerID());
            System.out.println("[1]  Add Dog");
            System.out.println("[2]  View Customer Dog List");
            System.out.println("[3]  Edit Dog");
            System.out.println("[4]  Delete Dog");
            System.out.println("[5]  View Services");
            System.out.println("[6]  Add Service for Dog");
            System.out.println("[7]  Show Total Amount");
            System.out.println("[8]  Pay Transaction");
            System.out.println("[9]  Logout");
            System.out.println("[10] Exit Program");

            System.out.println("===============================");
            System.out.println("Enter Choice:");
            int choice = scanner.nextInt();

            //User input 1 for customer registration
            if (choice == 1) {
                Registration.PetRegistration();

            //User input 2 for customer viewing his/her pets
            } else if (choice == 2) {
                viewCustomerPets();

            //User input 3 for customer editing his/her dogs
            } else if (choice == 3) {
                if(countDog()==0){
                    System.err.println("There are no Dogs");
                }else {
                    viewCustomerPets();
                    editDogName();
                }

            //User input 4 for customer deleting his/her dog
            } else if (choice == 4) {
                if(countDog()==0){
                    System.err.println("There are no Dogs");
                }else {
                    viewCustomerPets();
                    deleteDog();
                }


            //User input 5 for customer viewing the list of available services
            } else if (choice == 5) {
                CustomerMenu customerMenu = new CustomerMenu();
                ArrayList<Services> customerServices;
                customerServices = customerMenu.viewCustomerServices();
                System.out.println();
                System.out.println("List of Services: ");
                for (Services services : customerServices) {
                    System.out.printf("%-5s %-15s Php%-15f %-15s %-15s %-10s \n", services.getServiceID(),
                            services.getServiceName(), services.getPrice(),services.getServiceStatus(),
                            services.getServiceTime(),services.getServiceDescription());
                }

            //User input 6 to add service for his/her dog
            }else if(choice ==6){
                if(countDog()==0){
                    System.err.println("There are no Dogs");
                }else {
                    viewCustomerPets();
                    addServiceForADog();
                }


            //User input 7 to show the total price of services
            }else if(choice==7){

                System.out.println("Total Amount to pay: " + getSumAmount());

            //User input 8 for User to pay his/her transaction
            }else if(choice == 8){
                getSumAmount();
                System.out.println(sumAmount);
                if(sumAmount==0){
                    System.out.println("Your Balance is 0, No need to pay yet!");
                    customerMenu();
                }else {
                    Scanner scanner1 = new Scanner(System.in);
                    System.out.println("Would you like to pay your transactions? ");
                    System.out.println("Y/N: ");
                    String input = scanner1.nextLine();
                    if(input.equals("Y")||input.equals("y")){
                        payTransactions();
                    }else if (input.equals("N")||input.equals("yn")){
                        customerMenu();
                }

                }
            //User input 9 for the user to logout
            }else if(choice == 9){
                RunDogTrack run = new RunDogTrack();
                run.LoginAndRegistrationMenu();
            //User inpuut 10 for the user to exit the program
            }else if(choice == 10){
                System.exit(0);
            }

        }

    }

    //the code/method below views the list of services
    public static ArrayList<Services> viewCustomerServices() throws SQLException {
        ArrayList<Services> arrayList = new ArrayList<Services>();
        //String query = "SELECT * FROM `the_app_users` WHERE `u_uname` =? AND `u_pass` =?";
        String sql = "SELECT * FROM `services`";
        PreparedStatement preparedStatement = customerMenuConnection.prepareStatement(sql);
        ResultSet rs = preparedStatement.executeQuery(sql);

        while (rs.next()) {
            Services services = new Services(rs.getString(1), rs.getString(2),
                    rs.getString(3), rs.getInt(4),rs.getString(5),
                    rs.getString(6));

            arrayList.add(services);
        }
        return arrayList;
    }

    //The method/code below vies the customer's list pets
    public static void viewCustomerPets() throws SQLException {

        String sql = "SELECT * FROM customerandpets WHERE EmailAddress = '"+Login.getCustomerEmail()+"'";
        PreparedStatement stmt = customerMenuConnection.prepareStatement(sql);

        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next())
        {

            String PetName = rs.getString("PetName");
            String Name = rs.getString("Name");
            String EmailAdress = rs.getString("EmailAddress");


            // print the results
            System.out.format("%s, %s, %s\n", PetName, Name, EmailAdress);
        }

    }


    //the code/method below edits the name of the dog by entering his/her dog
    public static void editDogName() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Dog Name to be Updated: ");
        String petName = scanner.nextLine();

        System.out.println("Enter Dog New Name: ");
        String newpetName = scanner.nextLine();


        String sql = "UPDATE customerandpets SET PetName = '" + newpetName + "' WHERE EmailAddress = '"
                +Login.getCustomerEmail()+"' AND PetName = '" + petName + "'";

        PreparedStatement preparedStatement = customerMenuConnection.prepareStatement(sql);
        preparedStatement.executeUpdate();
    }

    //the code/method below deletes the name of the dog
    public static void deleteDog() throws SQLException {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Dog Name you want to Delete: ");
        String petName = scanner.nextLine();


        String sql = "DELETE FROM dog WHERE PetName ='" + petName+"'";
        PreparedStatement preparedStatement = customerMenuConnection.prepareStatement(sql);
        preparedStatement.executeUpdate();

    }

    //the method/code below add service for a dog
    public static void addServiceForADog() throws SQLException {

        String transactionID;
        Login login = new Login();
        Services services = new Services();
        Dog dog = new Dog();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome to Dog Track!");

        System.out.println("Enter Dog Name: ");
        petName = scanner.nextLine();
        dog.setPetName(petName);

        System.out.println("Enter Service ID: ");
        serviceID = scanner.nextLine();
        services.setServiceID(serviceID);

        System.out.println("Enter your desired employee ");
        System.out.println("Enter employeeID: ");
        employeeID = scanner.next();

        System.out.println("Service ID: "+getServiceID());
        System.out.println("Service Price: " + getServicePrice());
        System.out.println("Pet Name: "+ dog.getPetName());
        System.out.println("Number of Transactions for this Customer: " + countCustomer());
        //System.out.println("Employee Name: " + getEmployeeName());

           // note that i'm leaving "date_created" out of this insert statement

        String registerQuery = "INSERT INTO transactiondetails values(?,?,?,?,?)";
        PreparedStatement registerTransation = customerMenuConnection.prepareStatement(registerQuery);

        String latestTransactionID = getLatestTransactionID();
        String[] splitToIncrement = latestTransactionID.split("T");
        int incrementValue = Integer.parseInt(splitToIncrement[1]);
        incrementValue++;
        transactionID = "T"+incrementValue;

        System.out.println("Transaction ID: " + transactionID);
        registerTransation.setString(1, transactionID);
        registerTransation.setString(2, getServiceID());
        registerTransation.setDouble(3, getServicePrice());
        registerTransation.setString(4, getPetID());
        registerTransation.setString(5, login.getCustomerID());
        registerTransation.executeUpdate();

        if(countCustomer()==0){
            String sql1 = "INSERT INTO transaction(TransactionID,Amount,CustomerID) "
                    +"VALUES ('"+transactionID+ "','"+getSumAmount()+
                    "','"+login.getCustomerID()+"')";
            System.out.println("Transaction Added");
            PreparedStatement preparedStatement1 = customerMenuConnection.prepareStatement(sql1);
            preparedStatement1.executeUpdate();
        }else{
            String sql2 = "UPDATE transaction SET Amount = '" + getSumAmount() + "' WHERE CustomerID = '"
                    +Login.getCustomerID()+"'";
            System.out.println("Transaction Updated");
            PreparedStatement preparedStatement2 = customerMenuConnection.prepareStatement(sql2);
            preparedStatement2.executeUpdate();
        }

        System.out.println("Service for Dog Booked");
        System.out.println("Back to the Menu");

        customerMenu();
    }

    public static String getLatestTransactionID() throws SQLException {

        Statement stmt = customerMenuConnection.createStatement();
        String sql = ("SELECT transactiondetails.transactionid FROM transactiondetails ORDER BY transactiondetails.transactionid DESC LIMIT 1;");

        ResultSet rs = stmt.executeQuery(sql);

        if (rs.next()){
            transactionID = rs.getString(1);

        }

        return transactionID;
    }

    public static String getLatestPaymentID() throws SQLException {

        Statement stmt = customerMenuConnection.createStatement();
        String sql = ("SELECT paymentdetails.payno FROM paymentdetails ORDER BY paymentdetails.payno DESC LIMIT 1;");

        ResultSet rs = stmt.executeQuery(sql);

        if (rs.next()){
            paymentID = rs.getString(1);

        }

        return paymentID;
    }
    //returns Service ID inputted
    public static String getServiceID(){return  serviceID;}

    //returns Employee ID chosend by the customer
    public static String getEmployeeID(){return employeeID;}



    //returns chosen Employee Name
    public static String getEmployeeName() throws SQLException {

        String sql = "SELECT employee.EmployeeName FROM employee WHERE employee.EmployeeID = " +getEmployeeID();

        PreparedStatement stmt = customerMenuConnection.prepareStatement(sql);

        ResultSet rs = stmt.executeQuery(sql);

        while (rs.next()) {
            employeeName = rs.getString(1);
        }
        return employeeName;
    }

    //the code below if for the transaction of payments
    public static void payTransactions() throws SQLException {
        Login login = new Login();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Your Total Amount is: " + getSumAmount());
        System.out.println("");
        System.out.println("Would you like to pay through Gcash or Cash: ");
        String paymentType = scanner.nextLine();

        String registerQuery = "INSERT INTO paymentdetails values(?,?,?,?)";
        PreparedStatement registerPayment = customerMenuConnection.prepareStatement(registerQuery);

        String latestPaymentID = getLatestPaymentID();
        String[] splitToIncrement = latestPaymentID.split("PA");
        int incrementValue = Integer.parseInt(splitToIncrement[1]);
        System.out.println(incrementValue);
        incrementValue++;
        if(incrementValue>11){
            incrementValue++;
        }
        System.out.println(incrementValue);
        paymentID = "PA"+incrementValue;

        System.out.println(paymentID);
        registerPayment.setString(1, paymentID);
        registerPayment.setString(2, getTransactionID(login.getCustomerID()));
        registerPayment.setString(3, paymentType);
        registerPayment.setDouble(4, getSumAmount());

        registerPayment.executeUpdate();
        System.out.println("PAID. THANK YOU PLEASE COME AGAIN");
        System.out.println(login.getCustomerID());



        if(countPayment()==0){

            String sql1 = "INSERT INTO payment(payno, paymentamount, employeeid, customerid) "
                    +"VALUES ('"+paymentID+"','"+getSumAmount()+ "', '"+getEmployeeID()+"', '"+login.getCustomerID()+ "')";
            PreparedStatement preparedStatement1 = customerMenuConnection.prepareStatement(sql1);
            preparedStatement1.executeUpdate();

        }else{
            String sql2 = "UPDATE payment SET paymentamount = " + getSumAmount() + " WHERE CustomerID = '"
                    +Login.getCustomerID()+"'";
            PreparedStatement preparedStatement1 = customerMenuConnection.prepareStatement(sql2);
            preparedStatement1.executeUpdate();


        }

    }

    public static int countPayment() throws SQLException {
        Login login = new Login();

        String sql1 = "SELECT COUNT(payment.Customerid) FROM payment WHERE payment.Customerid = '" + login.getCustomerID()+"'";
        PreparedStatement preparedStatement1 = customerMenuConnection.prepareStatement(sql1);

        ResultSet rs = preparedStatement1.executeQuery(sql1);

        while(rs.next()){
            numberOfPayment = rs.getInt(1);
        }
        return numberOfPayment;

    }

    public static String getTransactionID(String customerID) throws SQLException {
        Login login = new Login();

        String sql1 = "SELECT transaction.TransactionID FROM transaction" +
                " WHERE transaction.CustomerID=  '" + customerID+"' ";
        PreparedStatement preparedStatement1 = customerMenuConnection.prepareStatement(sql1);

        ResultSet rs = preparedStatement1.executeQuery(sql1);

        while(rs.next()){
            transactionID = rs.getString(1);
        }
        return transactionID;
    }

    //the method below counts the number of customer in a table
    public static int countCustomer() throws SQLException {

        Login login = new Login();

        java.sql.Statement stmt = customerMenuConnection.createStatement();
        String sql = "SELECT COUNT(transaction.CustomerID) FROM " +
                "transaction WHERE transaction.CustomerID = '"+login.getCustomerID()+"'";

        stmt.execute(sql);

        ResultSet rs = stmt.executeQuery(sql);

        while(rs.next()){
            numberOfCustomer = rs.getInt(1);
        }
        return numberOfCustomer;
    }

    public static int countDog() throws SQLException {
        Login login = new Login();

        java.sql.Statement stmt = customerMenuConnection.createStatement();
        String sql = "SELECT COUNT(dog.PetID) FROM " +
                "dog WHERE dog.CustomerID = '"+login.getCustomerID()+"'";

        stmt.execute(sql);

        ResultSet rs = stmt.executeQuery(sql);

        while(rs.next()){
            numberOfDog = rs.getInt(1);
        }
        return numberOfDog;
    }

    //the method/code below gets the sum amount of the payment for the customer
    public static double getSumAmount() throws SQLException {

        Login login = new Login();

        java.sql.Statement stmt = customerMenuConnection.createStatement();
        String sql = "SELECT payment.paymentamount FROM " +
                "payment WHERE payment.customerid = '"+login.getCustomerID()+"'";
        stmt.execute(sql);
        ResultSet rs = stmt.executeQuery(sql);

        while(rs.next()){
            sumAmount = rs.getInt(1);

        }

        return sumAmount;

    }

    //the code/method below returns the service price
    public static double getServicePrice() throws SQLException {
        Services services = new Services();

        java.sql.Statement stmt = customerMenuConnection.createStatement();
        String sql = "SELECT services.Price FROM " +
                "services WHERE ServiceID = '"+serviceID+"'";
        stmt.execute(sql);
        ResultSet rs = stmt.executeQuery(sql);

        while(rs.next()){
            servicePrice = rs.getDouble(1);

        }
        services.setPrice(servicePrice);
        return services.getPrice();
    }

    //the code/method below returns the pet id of the specific customer
    public static String getPetID() throws SQLException {
        Dog dog = new Dog();

        java.sql.Statement stmt = customerMenuConnection.createStatement();
        String sql = "SELECT dog.PetID FROM " +
                "dog WHERE PetName = '"+petName+"'";
        stmt.execute(sql);
        ResultSet rs = stmt.executeQuery(sql);

        while(rs.next()){
            petID = rs.getString(1);

        }
        dog.setPetID(petID);
        return dog.getPetID();
    }
}
