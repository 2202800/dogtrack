
import Object.Customer;
import Object.Dog;

import java.sql.*;
import java.util.Scanner;

/**
 * A Java MySQL INSERT example.
 * Demonstrates the use of a SQL INSERT statement against a
 * MySQL database, called from a Java program, using a
 * Java Statement (not a PreparedStatement).
 *
 * Created by Alvin Alexander, http://devdaily.com
 */
public class Registration
{
    private static Connection registerConnection;

    //the code below sets the connection to phpmyadmin
    public static void setConnection() {
        try {
            String connectionURL = "jdbc:mysql://localhost/dogtrack?user=root&password=";
            registerConnection = DriverManager.getConnection(connectionURL);

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    //the code below closes the connection to phpmyadmin
    public static void closeConnection() throws SQLException {
        if (registerConnection != null) {
            registerConnection.close();
        }
    }

    //the code below is for customer registration
    public static void CustomerRegistration(){
        try
        {
            // create a mysql database connection
            //String myUrl = "jdbc:mysql://localhost/dogtrack?user=root&password=";

           // Connection conn = DriverManager.getConnection(myUrl, "root", "");

            Statement st = registerConnection.createStatement();

            Customer customer = new Customer();
            String customerID;

            Scanner scanner = new Scanner(System.in);

            System.out.println("Welcome to Dog Track!");

            System.out.println("Enter Customer Name: ");
            String name = scanner.nextLine();
            customer.setName(name);

            System.out.println("Enter Customer Email Address: ");
            String emailAdress = scanner.nextLine();
            customer.setEmailAddress(emailAdress);

            System.out.println("Enter Customer Home Address: ");
            String homeAddress = scanner.nextLine();
            customer.setHomeAddress(homeAddress);

            System.out.println("Enter Customer Password");
            String password = scanner.nextLine();
            customer.setCustomerPassword(password);

            // note that i'm leaving "date_created" out of this insert statement
          //  st.executeUpdate("INSERT INTO customer(CustomerID, Name, EmailAddress, HomeAddress,CustomerPassword) "
         //           +"VALUES ('0','"+customer.getName()+ "', '"+customer.getEmailAddress()+ "', '"+customer.getHomeAddress()+ "', '"+customer.getCustomerPassword()+ "')");

            String registerQuery = "INSERT INTO customer values(?,?,?,?,?)";
            PreparedStatement registerCustomer = registerConnection.prepareStatement(registerQuery);

            String latestCustomerID = getLatestCustomerID();
            String[] splitToIncrement = latestCustomerID.split("C");
            int incrementValue = Integer.parseInt(splitToIncrement[1]);
            incrementValue++;
            customerID = "C"+incrementValue;

            //System.out.println(customerID);
            registerCustomer.setString(1, customerID);
            registerCustomer.setString(2, name);
            registerCustomer.setString(3, emailAdress);
            registerCustomer.setString(4, homeAddress);
            registerCustomer.setString(5, password);
            registerCustomer.executeUpdate();

            System.out.println("Customer Registration Complete!");
            System.out.println("Back to the Menu");

            RunDogTrack run = new RunDogTrack();
            run.LoginAndRegistrationMenu();


           // registerConnection.close();
        }
        catch (Exception e)
        {
            //System.err.println("Got an exception!");
            //System.err.println(e.getMessage());
            System.err.println("Invalid");

        }
    }

    public static String getLatestCustomerID() throws SQLException {
        Customer customer = new Customer();
        String customerID = "";

        Statement stmt = registerConnection.createStatement();
        String sql = ("SELECT customer.customerID FROM customer ORDER BY customer.customerID DESC LIMIT 1;");

        ResultSet rs = stmt.executeQuery(sql);

        if (rs.next()){
            customerID = rs.getString(1);

        }
        customer.setCustomerId(customerID);
        //System.out.println(customerID);
        return customer.getCustomerId();
    }

    public static String getLatestDogID() throws SQLException {
        Dog dog = new Dog();
        String dogID = "";

        Statement stmt = registerConnection.createStatement();
        String sql = ("SELECT dog.petID FROM dog ORDER BY dog.petID DESC LIMIT 1;");

        ResultSet rs = stmt.executeQuery(sql);

        if (rs.next()){
            dogID = rs.getString(1);

        }
        dog.setPetID(dogID);
        //System.out.println(customerID);
        return dog.getPetID();
    }

    //the code below is for pet registration
    public static void PetRegistration(){
        try
        {
            String petID;
            // create a mysql database connection
          //  String myUrl = "jdbc:mysql://localhost/dogtrack?user=root&password=";

          //  Connection conn = DriverManager.getConnection(myUrl, "root", "");

            Statement st = registerConnection.createStatement();

            Login login = new Login();
            Dog dog = new Dog();

            Scanner scanner = new Scanner(System.in);

            System.out.println("Welcome to Dog Track!");

            System.out.println("Enter Pet Name: ");
            String petName = scanner.nextLine();
            dog.setPetName(petName);

            System.out.println("Enter DogBreed: ");
            String breed = scanner.nextLine();
            dog.setBreed(breed);

            System.out.println("Enter File Path for DogPicture");
            String dogPicture = scanner.nextLine();
            dog.setDogPicture(dogPicture);

            String registerQuery = "INSERT INTO dog values(?,?,?,?,?)";
            PreparedStatement registerDog = registerConnection.prepareStatement(registerQuery);

            String latestDogID = getLatestDogID();
            String[] splitToIncrement = latestDogID.split("P");
            int incrementValue = Integer.parseInt(splitToIncrement[1]);
            incrementValue++;
            petID = "P"+incrementValue;

            System.out.println(petID);
            registerDog.setString(1, petID);
            registerDog.setString(2, petName);
            registerDog.setString(3, breed);
            registerDog.setString(4, login.getCustomerID());
            registerDog.setString(5, dogPicture);
            registerDog.executeUpdate();

            RunDogTrack run = new RunDogTrack();
            run.LoginAndRegistrationMenu();

            System.out.println("PET REGISTERED! THANK YOU");
           // conn.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            //System.err.println("Got an exception!");
            //System.err.println(e.getMessage());
        }


    }

    }

