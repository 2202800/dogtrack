import Object.Customer;
import Object.Employee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Login {
    private static Connection loginConnection;
    private static String emailAddress;
    private static String customerID;
    private static String employeeID;

    //the code below sets the connection to phpmyadmin
    public static void setConnection() {
        try {
            String connectionURL = "jdbc:mysql://localhost/dogtrack?user=root&password=";
            loginConnection = DriverManager.getConnection(connectionURL);

        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    //the code below closes the connection from phpmyadmin
    public static  void closeConnection() throws SQLException {
        if (loginConnection != null) {
            loginConnection.close();
        }
    }

    //the method/code below is for customer login
    public static void CustomerLogin() {
        Scanner scanner = new Scanner(System.in);

        try {
            Customer customer = new Customer();

            System.out.println("Enter Email Address: ");
            emailAddress = scanner.nextLine();
            customer.setEmailAddress(emailAddress);
            getCustomerEmail();

            System.out.println("Enter Password: ");
            String customerPassword = scanner.nextLine();
            customer.setCustomerPassword(customerPassword);

            java.sql.Statement stmt = loginConnection.createStatement();
            String sql = "SELECT * FROM customer WHERE EmailAddress = '" + customer.getEmailAddress()
                    + "' AND CustomerPassword ='" + customer.getCustomerPassword() + "'";
            stmt.execute(sql);
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                CustomerMenu customerMenu = new CustomerMenu();
                System.out.println("Login Successful");
                CustomerMenu.setConnection();
                customerMenu.customerMenu();
            } else {
                System.err.println("Login Failed");
                System.err.println("PLEASE TRY AGAIN!");
                emailAddress=null;
            }

        } catch (SQLException x) {
            System.out.println(x);
        }
    }

    //The method below returns the customer email address inputted
    public static String getCustomerEmail(){
        return emailAddress;
    }

    //The method below returns the customer id
    public static String getCustomerID() throws SQLException {
        Customer customer = new Customer();

        java.sql.Statement stmt = loginConnection.createStatement();
        String sql = "SELECT customer.customerid FROM " +
                "customer WHERE customer.emailaddress = '"+ getCustomerEmail() + "'";
        stmt.execute(sql);
        ResultSet rs = stmt.executeQuery(sql);

        while(rs.next()){
           customerID = rs.getString(1);

        }
        customer.setCustomerId(customerID);
        return customer.getCustomerId();
    }

    //The method below returns the EmployeeID
    public static String getEmployeeID() {
        return employeeID;
    }

    //the code below is for employee login
    public static void EmployeeLogin() {
        Scanner scanner = new Scanner(System.in);

        try {
            Employee employee = new Employee();

            System.out.println("Enter Employee ID: ");
            employeeID = scanner.next();
            employee.setEmployeeID(employeeID);

            System.out.println("Enter Password: ");
            String employeePassword = scanner.next();
            employee.setEmployeePassword(employeePassword);

            java.sql.Statement stmt = loginConnection.createStatement();
            String sql = "SELECT * FROM employee WHERE EmployeeID = '" + employeeID + "' AND EmployeePassword ='" + employeePassword + "'";
            stmt.execute(sql);
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.next()) {
                System.out.println("Login Successful");
                EmployeeMenu.setConnection();
                EmployeeMenu.employeeMenu();
            } else {
                System.out.println("Login Failed");
            }

        } catch (SQLException x) {
            System.out.println(x);
        }
    }
}
